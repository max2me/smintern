﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Website.Model
{
	public class User
	{
		public string screen_name { get; set; }
		public int followers_count { get; set; }

		private sealed class ScreenNameEqualityComparer : IEqualityComparer<User>
		{
			public bool Equals(User x, User y)
			{
				if (ReferenceEquals(x, y)) return true;
				if (ReferenceEquals(x, null)) return false;
				if (ReferenceEquals(y, null)) return false;
				if (x.GetType() != y.GetType()) return false;
				return string.Equals(x.screen_name, y.screen_name);
			}

			public int GetHashCode(User obj)
			{
				return (obj.screen_name != null ? obj.screen_name.GetHashCode() : 0);
			}
		}

		public override string ToString()
		{
			return string.Format("{0} ({1})", screen_name, followers_count);
		}


		private static readonly IEqualityComparer<User> ScreenNameComparerInstance = new ScreenNameEqualityComparer();

		public static IEqualityComparer<User> ScreenNameComparer
		{
			get { return ScreenNameComparerInstance; }
		}

		protected bool Equals(User other)
		{
			return string.Equals(screen_name, other.screen_name);
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != this.GetType()) return false;
			return Equals((User) obj);
		}

		public override int GetHashCode()
		{
			return (screen_name != null ? screen_name.GetHashCode() : 0);
		}
	}
}
