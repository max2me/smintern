﻿using System;

namespace SMI.Twitter
{
	public class Status
	{
		public string text { get; set; }
		public int favorite_count { get; set; }
		public int retweet_count { get; set; }
		public long id { get; set; }
		public User User { get; set; }

		public string created_at { get; set; }
		public DateTime created
		{
			get
			{
				if (String.IsNullOrEmpty(created_at))
					return DateTime.MinValue;

				return created_at.ToDateTime();
			}
		}

		public double Score
		{
			get
			{
				int follows = User.followers_count;

				return (double)favorite_count/follows + (double)retweet_count*2/follows;
			}
		}

		public override string ToString()
		{
			return string.Format("text: {0},\nretweet_count: {1},\nfavorite_count: {2},\nScore: {3},\nUser: {4}\n\n", text, retweet_count, favorite_count, Score, User.screen_name);
		}
	}
}