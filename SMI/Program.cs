﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;
using RestSharp.Authenticators;
using SMI.Twitter;

namespace SMI
{
	class Program
	{
//		private static readonly string[] Topics = new string[] {"design", "user experience", "technology", "startups"};
		private static readonly string[] Topics = new string[] {"interior design", "decorations", "styles", "ideas"};
		private static readonly TimeSpan RecencyThreshold = new TimeSpan(0, 12, 0, 0);

		static void Main(string[] args)
		{
			var tweets = new List<Status>();

			foreach (var topic in Topics)
			{
				var users = GetTopUsersBySearch(topic);
				var topUsers = GetSliceByFollowers(users, 0.1, 0.5).Concat(GetSliceByFollowers(users, 0.7, 0.1));

				foreach (var user in topUsers)
				{
					var recentStatuses = GetStatuses(user, 100);

					recentStatuses = recentStatuses
										.Where(s => DateTime.Now - s.created < RecencyThreshold)
										.Where(s => s.text.Contains("http://"))
										.Where(s => s.Score > 0)
										.OrderByDescending(s => s.Score)
										.Take(1)
										.ToList();

					tweets.AddRange(recentStatuses);
				}
			}


			var selected = tweets.OrderByDescending(s => s.Score).Take(10).ToList();

			foreach (var s in selected)
			{
				W(s.ToString());
			}

			W("Press any key to continue...");
			Console.ReadLine();
		}


		private static List<User> GetTopUsersBySearch(string topic)
		{
			var request = new RestRequest("users/search.json");
			request.AddParameter("q", topic);
			request.AddParameter("page", 1);
			request.AddParameter("count", 30);
			request.Method = Method.GET;

			var resp = Client.Instance.Execute<List<User>>(request);

			return resp.Data;
		}

		private static List<Status> GetStatuses(User user, int count)
		{
			var request = new RestRequest("statuses/user_timeline.json");
			request.AddParameter("screen_name", user.screen_name);
			request.AddParameter("count", count);
			request.AddParameter("exclude_replies", true);
			request.AddParameter("include_rts", false);
			request.AddParameter("trim_user", true);
			request.Method = Method.GET;

			var resp = Client.Instance.Execute<List<Status>>(request);

			foreach (var tweet in resp.Data)
			{
				tweet.User = user;
			}

			return resp.Data;
		}

		private static List<User> GetSliceByFollowers(List<User> users, double skip, double take)
		{
			var ptake = (int) Math.Round(users.Count*take);
			var pskip = (int) Math.Round(users.Count*skip);

			return users.OrderByDescending(u => u.followers_count).Skip(pskip).Take(ptake).ToList();
		}

		private static void W(string s)
		{
			Console.WriteLine(s);
		}

		private static void W(List<object> list)
		{
			foreach (var o in list)
				W(o.ToString());
		}
	}
}
